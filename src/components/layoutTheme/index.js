import React, { Component } from 'react';

import {Breadcrumb} from "antd";
import {Layout} from "antd";

import SideMenu from '../sideMenu';
import Head from '../head'
import Foot from '../foot';

const { Content } = Layout;

export default class LayoutTheme extends Component{
    render() {
        const { children } = this.props;

        return(
            <Layout>
                <Head/>

                <Content style={{ padding: '0 50px' }}>

                    <Breadcrumb style={{ margin: '16px 0' }}>
                        <Breadcrumb.Item>Home</Breadcrumb.Item>
                        <Breadcrumb.Item>List</Breadcrumb.Item>
                        <Breadcrumb.Item>App</Breadcrumb.Item>
                    </Breadcrumb>

                    <Layout style={{ padding: '24px 0', background: '#fff' }}>
                        <SideMenu/>

                        <Content style={{ padding: '0 24px', minHeight: '420px' }}>

                            { children }

                        </Content>

                    </Layout>

                </Content>

                <Foot/>

            </Layout>
        );
    }
}
