import React, { Component, Fragment } from 'react';

import { Form, Icon, Input, Button, DatePicker } from 'antd';
import SelectComponent from '../SelectComponent';
import axios from 'axios';
import api from "../../services/api";

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Creators as CadastroActions } from '../../store/ducks/cadastro';

const dateFormat = 'DD/MM/YYYY';

class Cadastro extends Component {

    state = {
        nome: '',
        email: '',
        nascimento: '',
        /*estados: [],
        cidades: [],
        estadoSelecionado: {

        },
        cidadeSelecionada: {

        },*/
    };

    /*componentDidMount() {
        axios.get('https://servicodados.ibge.gov.br/api/v1/localidades/estados')
            .then(res => {
                const estadoInicial = res.data;
                this.setState({estados: estadoInicial});
        });

    }

    selectEstado = async (value) => {
        let codigoEstado = this.state.estados[value].id;
        const {data: cidade} = await api.get(`localidades/estados/${codigoEstado}/municipios`);

        this.setState({
            estadoSelecionado: {
                nomeEstado: this.state.estados[value].nome,
                idEstado: this.state.estados[value].id,
            },
            cidades: cidade
        });

    };

    selectCidade = (value) => {
        this.setState({
            cidadeSelecionada: {
                nomeCidade: this.state.cidades[value].nome,
                idCidade: this.state.cidades[value].id,
            }
        },()=>(
            console.log(`Resultado: ${this.state.cidadeSelecionada}`)
        ));
    }; */

    handleSubmit = (e) => {
        e.preventDefault();

        this.props.addCadastro(
            this.state.nome,
            this.state.email,
            this.state.nascimento);

        this.setState({
            nome: '',
            email: '',
        });

        /*alert(`Nome: ${this.state.nome} \n 
                Email: ${this.state.email} \n 
                Estado: ${this.state.estadoSelecionado.nomeEstado} \n
                Cidade: ${this.state.cidadeSelecionada.nomeCidade} \n
                Nascimento: ${this.state.data} `); */
    };

    onChangeDate = (date, dateString) => {
        this.setState({
            nascimento: dateString,
        });

        console.log(date, dateString);
    };

    render() {
        console.log(this.props);
        return(
            <Fragment>
                <Form
                    layout="horizontal"
                    onSubmit={this.handleSubmit}
                    className="login-form"
                >
                    <Form.Item>
                        <Input
                            style={{ width: '400px'}}
                            prefix={<Icon type="user" style={{color: 'rgba(0,0,0,.25)'}} />}
                            placeholder="Insira seu nome"
                            value={this.state.nome}
                            onChange={e => this.setState({nome: e.target.value})}
                        />
                    </Form.Item>

                    <Form.Item>
                        <Input
                            style={{ width: '400px'}}
                            prefix={<Icon type="mail" style={{color: 'rgba(0,0,0,.25)'}} />}
                            placeholder="Seu email"
                            value={this.state.email}
                            onChange={e => this.setState({email: e.target.value })}
                            />
                    </Form.Item>
                    <Form.Item>
                        <DatePicker placeholder="Data de Nascimento" onChange={this.onChangeDate} format={dateFormat}/>
                    </Form.Item>

                    {/*<Form.Item>
                        <SelectComponent
                            style={{ width: '400px'}}
                            children={this.selectEstado}
                            value={this.state.estados} />
                    </Form.Item>
                    
                    <Form.Item>
                        <SelectComponent children={this.selectCidade} value={this.state.cidades}/>
                    </Form.Item> */}

                    <Button type="primary" htmlType="submit">

                        Confirmar
                    </Button>
                </Form>
            </Fragment>
        );
    }
}

const mapStateToProps = state => ({
    cadastro: state.cadastro,
});

const mapDispatchToProps = dispatch => bindActionCreators(CadastroActions, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Cadastro);
