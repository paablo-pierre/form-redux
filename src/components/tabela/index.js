import React, { Component } from 'react';

import { Table, Divider, Tag } from "antd";

import { connect } from "react-redux";

const columns = [{
    title: 'Nome',
    dataIndex: 'nome',
    key: 'nome',
}, {
    title: 'Email',
    dataIndex: 'email',
    key: 'email',
}, {
    title: 'Nascimento',
    dataIndex: 'nascimento',
    key: 'nascimento',
}];

class Tabela extends Component{
    render() {

        return (
            <Table columns={columns} rowKey="id" dataSource={this.props.children} />
        );
    }
}

export default Tabela;
