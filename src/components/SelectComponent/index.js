import React, { Component } from 'react';
import { Select } from 'antd';

const Option = Select.Option;

export default class SelectComponent extends Component {
    render() {
        let { value, children } = this.props;
        let optionItems = value.map((e, index) =>
            <Option key={index}> {e.nome} </Option>
        );
        return (
            <Select onChange={children} style={{ width: '400px'}}>
                {optionItems}
            </Select>

        );
    }
}