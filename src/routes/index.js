import React from 'react';

import { BrowserRouter, Switch, Route } from "react-router-dom";

import Main from '../pages/main';
import Usuarios from '../pages/usuarios';
import Estudo from '../pages/estudo';

const Routes = () => (
    <BrowserRouter>
        <Switch>
            <Route exact path='/' component={ Main } />
            <Route path='/user' exact={true} component={ Usuarios } />
            <Route path='/estudo' component={ Estudo }/>
        </Switch>
    </BrowserRouter>
);

export default Routes;