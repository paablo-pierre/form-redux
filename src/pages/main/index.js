import React, { Component } from 'react';
import LayoutTheme from "../../components/layoutTheme";
import Cadastro from "../../components/cadastro";

export default class Main extends Component {
    render() {
        return(
            <LayoutTheme>
               <Cadastro/>
            </LayoutTheme>
        );
    }
}
