import React, { Component } from 'react';

import Tabela from '../../components/tabela'
import LayoutTheme from "../../components/layoutTheme";

import { connect} from "react-redux";

class Usuarios extends Component {
    render() {
        return(
            <LayoutTheme>
                <Tabela children={this.props.cadastro}/>
            </LayoutTheme>
        );
    }
}

const mapStateToProps = state => ({
    cadastro: state.cadastro,
});

export default connect(mapStateToProps)(Usuarios);