import React, { Component } from 'react';

import LayoutTheme from "../../components/layoutTheme";

import {Form, DatePicker, Input, Checkbox } from "antd";
import Tabela from "../../components/tabela";

const dateFormat = 'DD/MM/YYYY';

class Estudo extends Component {

    state = {
        assunto: '',
        diaDeEstudo: '',
        feito: false,
    };

    handleSubmit = (e) => {
        e.preventDefault();
    };

    onChangeDate = (date, dateString) => {
      this.setState({
          diaDeEstudo: dateString,
      });
    };

    onChange = (e) => {
        console.log(`checked = ${e.target.checked}`);
    }


    render() {


        return(
            <LayoutTheme>
                <Form
                    onSubmit={this.handleSubmit}
                    layout='horizontal'
                >
                    <Form.Item>
                        <Input
                            placeholder='Assunto'
                            value={this.state.assunto}
                            onChange={e => this.setState({assunto: e.target.value})}
                        />
                    </Form.Item>

                    <Form.Item>
                        <DatePicker placeholder='Dia de Estudo' onChange={this.onChangeDate} format={dateFormat}/>
                    </Form.Item>

                    <Form.Item>
                        <Checkbox onChange={this.onChange}>
                            Feito
                        </Checkbox>
                    </Form.Item>
                </Form>

            </LayoutTheme>
        );
    }
}

export default Estudo;