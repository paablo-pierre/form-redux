/*
 * Types
 */

export const Types = {
    ADD_ESTUDO: 'estudo/ADD_ESTUDO',
};

/*
 * REDUCER
 */

const INITIAL_STATE = [];

export default function estudo(state = INITIAL_STATE, action) {

    switch (action.type) {
        case Types.ADD_ESTUDO:
            return[
                ...state, {
                    id: Math.random(),
                    assunto: action.payload.assunto,
                    diaDeEstudo: action.payload.diaDeEstudo,
                }
            ];
        default:
            return state;

    }
};

export const Creators = {
    addEstudo: (assunto, diaDeEstudo) => ({
        type: Types.ADD_ESTUDO,
        payload: { assunto, diaDeEstudo},
    })
};