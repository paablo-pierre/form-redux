import { combineReducers } from 'redux';

import cadastro from './cadastro';
import estudo from './estudo';

export default combineReducers ({
    cadastro, estudo
});