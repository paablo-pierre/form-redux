/* 
 * Types
*/

export const Types = {
    ADD_CADASTRO: 'cadastro/ADD_CADASTRO',
};

/* 
 * REDUCER
*/

const INITIAL_STATE = [];

export default function cadastro (state = INITIAL_STATE, action) {
    switch(action.type) {

        case Types.ADD_CADASTRO:
            return [ ...state, {
                id: Math.random(),
                nome: action.payload.nome,
                email: action.payload.email,
                nascimento: action.payload.nascimento,
        }];
        default:
            return state;
    }    
}

export const Creators = {
    addCadastro: (nome, email, nascimento) => ({
        type: Types.ADD_CADASTRO,
        payload: { nome, email, nascimento },
    })
};